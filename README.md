## Tarea 1 de Arquitectura de Software.

Proyecto desarrollado en JavaScript que entrega resultados respecto a métricas utilizadas en arquitectura de software.

Se consideran las métricas de grado de abstracción e inestabilidad de un repositorio escrito en lenguaje de Java.

Al obtener ambos datos, se exporta una imagen .png con el gráfico de distancia de la secuencia principal.

### Instalación.

Para ejecutar el proyecto, se deben instalar las dependencias.

`npm install`

Se genera la carpeta node_modules.

Para ejecutar se necesita ingresar el siguiente código en consola o terminal.

 `npm run dev ruta_proyecto/`

ruta_proyecto/ hace referencia a la ruta en donde se encuentra el repositorio Java que se quiere examinar.

### Bloques lógicos.

Para obtener el grado de abstracción de las distintas clases del repositorio, se utilizó la librería javaParse que permite contabilizar los artefactos abstractos y los concreto, entregando el valor de abstracción.

En el caso de encontrar la inestabilidad, se consideró los valores del acoplamiento.

Finalmente, se genera un gráfico con los valores obtenidos anteriormente, para visualizar la distancia de la secuencia principal entre la abstracción y la inestabilidad.

### Tecnologías utilizadas.

-Node.js


### Contribución

Saúl Cifuentes Oyarzo.
